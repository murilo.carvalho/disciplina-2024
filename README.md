# Arquivos para aula de processamento em Python e analise em R
## inclui exemplos
## inclui dataset do Git da Anna Klenn:
Baixe o dataset "CellAtlas_Subset" em 

https://github.com/ahklemm/ImageJMacro_Introduction/tree/711fc64f901154aa70bc9d8d518fda4246f4ffb5/CellAtlas_Subset
## Inclui bibliotecas pré-instaladas no R para o Colab:
 Por favor, faça o download das libs para R (colab), em arquivo .zip em

https://drive.google.com/file/d/1YVAuVSZNZQQrdZjDY_-sLu5blOqgWN9w/view?usp=share_link
